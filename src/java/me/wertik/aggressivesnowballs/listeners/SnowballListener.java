package me.wertik.aggressivesnowballs.listeners;

import me.wertik.aggressivesnowballs.Main;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ProjectileHitEvent;

public class SnowballListener implements Listener {

    @EventHandler
    public void onHit(ProjectileHitEvent e) {
        if (e.getEntity().getType().equals(EntityType.SNOWBALL)) {
            if (e.getHitEntity() != null) {

                Entity shooter = (Entity) e.getEntity().getShooter();

                if (e.getHitEntity().getType().equals(EntityType.PLAYER) && shooter.getType().equals(EntityType.PLAYER))
                    if (Main.getInstance().isPluginState()) {

                        Damageable damageable = (Damageable) e.getHitEntity();
                        damageable.damage(Main.getInstance().getSnowballDamage());

                        Player playerShooter = (Player) shooter;

                        // Run commands
                        for (String command : Main.getInstance().getCommands()) {
                            Main.getInstance().getServer().dispatchCommand(Main.getInstance().getServer().getConsoleSender(), Main.getInstance().parse(command, e.getHitEntity().getName(), playerShooter.getName()));
                        }
                    }
            }
        }
    }
}
