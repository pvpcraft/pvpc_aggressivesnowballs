package me.wertik.aggressivesnowballs;

import org.bukkit.configuration.file.FileConfiguration;

import java.io.File;
import java.util.List;

public class ConfigLoader {

    private Main plugin;
    private FileConfiguration config;

    public ConfigLoader() {
        plugin = Main.getInstance();
    }

    public void loadYamls() {

        // CF
        File configFile = new File(plugin.getDataFolder() + "/config.yml");

        if (!configFile.exists()) {
            plugin.getConfig().options().copyDefaults(true);
            plugin.saveConfig();
            plugin.getServer().getConsoleSender().sendMessage(plugin.getDescription().getName() + "§aGenerated default §f" + configFile.getName());
        }

        config = plugin.getConfig();
    }

    public double getSnowballDamage() {
        return config.getDouble("snowball-damage");
    }

    public boolean getPluginState() {
        return config.getBoolean("plugin-state");
    }

    public List<String> getCommands() {
        return config.getStringList("commands-on-hit");
    }

    public void save() {
        config.set("plugin-state", Main.getInstance().isPluginState());
        config.set("snowball-damage", Main.getInstance().getSnowballDamage());
        Main.getInstance().getConfig().options().copyDefaults(true);
        Main.getInstance().saveConfig();
    }
}
