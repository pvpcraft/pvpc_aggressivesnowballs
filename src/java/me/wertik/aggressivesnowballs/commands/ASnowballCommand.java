package me.wertik.aggressivesnowballs.commands;

import me.wertik.aggressivesnowballs.Main;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class ASnowballCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (command.getName().equalsIgnoreCase("asnowball")) {
            if (sender.hasPermission("asnowballs.admin"))
            if (args.length > 0) {
                if (args[0].equalsIgnoreCase("reload"))
                {
                    sender.sendMessage("§aReloaded.");
                    Main.getInstance().reloadConfig();
                    Main.getInstance().reload();
                } else if (args[0].equalsIgnoreCase("on")) {
                    if (args.length < 1 || args.length > 1) {
                        sendHelp(sender);
                        return false;
                    }
                    Main.getInstance().setPluginState(true);
                    sendMessage(sender, "§aToggled §fon");
                } else if (args[0].equalsIgnoreCase("off")) {
                    if (args.length < 1 || args.length > 1) {
                        sendHelp(sender);
                        return false;
                    }

                    Main.getInstance().setPluginState(false);
                    sendMessage(sender, "§aToggled §foff");
                } else if (args[0].equalsIgnoreCase("setdamage")) {

                    if (args.length < 2 || args.length > 2) {
                        sendHelp(sender);
                        return false;
                    }

                    double damage = 0;
                    try {
                        damage = Double.valueOf(args[1]);
                    } catch (NumberFormatException e) {
                        sendMessage(sender, "§cThat is not a number.");
                    }

                    sendMessage(sender, "§aDamage set to §f" + damage);
                    Main.getInstance().setSnowballDamage(damage);
                } else {
                    sendHelp(sender);
                    sender.sendMessage(args[0]);
                }
            } else
                sendHelp(sender);
        }
        return false;
    }

    private void sendHelp(CommandSender sender) {
        sender.sendMessage("§c/asnowball reload\n" +
                "\n§c/asnowball setdamage <double>" +
                "\n§c/asnowball on" +
                "\n§c/asnowball off");
    }

    private void sendMessage(CommandSender sender, String msg) {
        sender.sendMessage(Main.getInstance().getDescription().getName() + " " + msg);
    }
}
