package me.wertik.aggressivesnowballs;

import me.wertik.aggressivesnowballs.commands.ASnowballCommand;
import me.wertik.aggressivesnowballs.listeners.SnowballListener;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.List;

public class Main extends JavaPlugin {

    private static Main instance;

    public static Main getInstance() {return instance;}

    private ConfigLoader configLoader;
    private boolean pluginState;
    private double snowballDamage;
    private List<String> commands;

    @Override
    public void onEnable() {
        info("§f-------------");

        instance = this;
        configLoader = new ConfigLoader();
        getServer().getPluginManager().registerEvents(new SnowballListener(), this);
        getCommand("asnowball").setExecutor(new ASnowballCommand());
        info("§aClasses loaded");

        configLoader.loadYamls();
        pluginState = getConfigLoader().getPluginState();
        snowballDamage = getConfigLoader().getSnowballDamage();
        commands = getConfigLoader().getCommands();

        info("§aConfig loaded");

        info("§f-------------");
    }

    public void reload() {
        pluginState = getConfigLoader().getPluginState();
        snowballDamage = getConfigLoader().getSnowballDamage();
        commands = getConfigLoader().getCommands();
    }

    @Override
    public void onDisable() {
        getConfigLoader().save();
    }

    private void info(String msg) {
        getServer().getConsoleSender().sendMessage("§f["+getDescription().getName()+"] " + msg);
    }

    public ConfigLoader getConfigLoader() {
        return configLoader;
    }

    public boolean isPluginState() {
        return pluginState;
    }

    public double getSnowballDamage() {
        return snowballDamage;
    }

    public void setPluginState(boolean pluginState) {
        this.pluginState = pluginState;
    }

    public void setSnowballDamage(double snowballDamage) {
        this.snowballDamage = snowballDamage;
    }

    public List<String> getCommands() {
        return commands;
    }

    public String parse(String msg, String playerName, String shooterName) {
        if (msg.contains("%player_hit%"))
            msg = msg.replace("%player_hit%", playerName);
        if (msg.contains("%player_shooter%"))
            msg = msg.replace("%player_shooter%", shooterName);
        return msg;
    }
}
